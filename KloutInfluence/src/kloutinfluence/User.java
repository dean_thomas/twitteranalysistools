/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package kloutinfluence;

/**
 *
 * @author 4thFloor
 */
public class User {
    public String twitterName;
    public String twitterScreenName;
    public String twitterID;
    public Boolean twitterVerified;
    public String kloutID;
    public String kloutScore;
    
    @Override
    public String toString()
    {
        String result = "";
        
        result += twitterID;
        result += ", ";
        result += twitterScreenName;
        result += ", ";
        result += twitterName;
        result += ", ";
        result += twitterVerified.toString();
        result += ", ";
        result += kloutID;
        result += ", ";
        result += kloutScore;
        
        return result;   
    }
}
