package kloutinfluence;

import com.klout4java.Klout4Java;
import com.klout4java.Klout4JavaException;
import com.klout4java.KloutConfig;
import com.klout4java.vo.InfluenceResponse;
import com.klout4java.vo.ScoreResponse;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author 4thFloor
 */
public class KloutInfluenceTool {

    KloutConfig config;
    Klout4Java klout;
    String inputFile;
    String outputFile;
    ArrayList<User> userList;

    public KloutInfluenceTool(String inputFile, String outputFile) {
        init();

        this.inputFile = inputFile;
        this.outputFile = outputFile;

        userList = readInputUserFile(inputFile);
        processUserList(userList);
        writeKloutScoreFile(outputFile, userList);
    }

    private Boolean writeKloutScoreFile(String outputFile, ArrayList<User> users) {
        try (FileWriter writer = new FileWriter(outputFile)) {
            //  File header
            writer.write("twitterID, screenName, Name, Verified, KloutID, KloutScore");
            writer.write("\r\n");

            for (int i = 0; i < users.size(); ++i) {
                writer.write(users.get(i).toString());
                writer.write("\r\n");
            }
            writer.close();

            return true;
        } catch (IOException ex) {
            Logger.getLogger(KloutInfluenceTool.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private void processUserList(ArrayList<User> userList) {
        for (int i = 0; i < userList.size(); ++i) {
            //  Retreive the user's Klout ID
            String kloutID = GetKloutIDForTwitterID(userList.get(i).twitterID);
            userList.get(i).kloutID = kloutID;

            //  Retreive the user's Klout Score
            String kloutScore = getInfluenceFromKloutID(kloutID);
            userList.get(i).kloutScore = kloutScore;

            System.out.println(userList.get(i).toString());
            System.out.println();
        }
    }

    private ArrayList<User> readInputUserFile(String inputFile) {
        ArrayList<User> result = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            //  Read the header and ignore
            String currentLine = reader.readLine();

            while ((currentLine = reader.readLine()) != null) {
                //  Create a new record for the user
                User newUser = new User();

                //  Split the current line using comma separators
                String[] currentLineValues = currentLine.split(",");

                //  Users who delete their twitter account will be marked
                //  as "NOT FOUND" in the user file
                if (!currentLineValues[0].equalsIgnoreCase("NOT FOUND")) {
                    //  User id is the first value
                    Long twitterId = Long.parseLong(currentLineValues[0]);
                    newUser.twitterID = twitterId.toString();

                    //  Screen name (@user) is the second value
                    String twitterScreenName = currentLineValues[1];
                    newUser.twitterScreenName = twitterScreenName;

                    //  Users actual name is the third value
                    String twitterName = currentLineValues[2];
                    newUser.twitterName = twitterName;

                    //  User verified status is the eighth value
                    Boolean twitterVerified = Boolean.parseBoolean(currentLineValues[7]);
                    newUser.twitterVerified = twitterVerified;

                    result.add(newUser);
                }
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(KloutInfluenceTool.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KloutInfluenceTool.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    private String getInfluenceFromKloutID(String kloutID) {
        ScoreResponse score = null;
        try {
            score = klout.score(kloutID);
        } catch (Klout4JavaException e) {
            e.printStackTrace();
        }

        if (score != null) {
            return score.getScore();
        } else {
            return "";
        }
    }

    private void init() {
        config = new KloutConfig();
        klout = new Klout4Java();

        config.setApiKey("rj96unw3gxjaknbngcqtqawh");
        klout.setConfig(config);
    }

    private String GetKloutIDForTwitterID(String twitterID) {

        String kloutID = "";
        try {
            kloutID = klout.kloutIDForTwitterID(twitterID);
        } catch (Klout4JavaException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return kloutID;
    }

    private String GetUserIDForTwitterName(String twitterName) {
        String kloutID = "";

        try {
            kloutID = klout.kloutIDForTwScreenName(twitterName);
        } catch (Klout4JavaException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return kloutID;
    }
}
