/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package randomtweetdownloader;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import twitter4j.HashtagEntity;
import twitter4j.TwitterException;
import twitter4j.URLEntity;

/**
 *
 * @author 4thFloor
 */
public class RandomTweetDownloader {

    static String OUTPUT_DIR = "C:\\";
    static String LOG_FILE = "twitter_stream.log";
    static String RANDOM_TWEETS_FILENAME = "twitter_random";
    
    static int TOTAL_TWEETS = 1000;
    static int TWEETS_PER_FILE = 500;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        if (!setOutputFile(LOG_FILE))
        {
            System.err.println("Unable to set output to file: %s.  Logging"
                    + " will continue through the system console.  Note:"
                    + " this may slow down execution and cause the application"
                    + " to respond slowly on large collections.");
        }
        
        CollectRandomTweets(TOTAL_TWEETS, TWEETS_PER_FILE);
    }
    
    /**
     * Twitter4j creates a huge amount of debug data as it collects - reroute
     * this to a file.  As there is so much data written to the console by 
     * Twitter4j it can lead to slowdown or crashing of the console in IDEs
     * such as NetBeans or Eclipse.  To avoid this we dump it to a text file
     * instead.
     * @param filename: the file to be used for the debug output.
     * @return Boolean: true if the file was successfully created.
     */
    static Boolean setOutputFile(String filename)
    {
        try
        {
            PrintStream ps = new PrintStream(OUTPUT_DIR + filename);
            System.setOut(ps);
            return true;
        } 
        catch (FileNotFoundException ex) 
        {
            Logger.getLogger(RandomTweetDownloader.class.getName()).log(
                    Level.SEVERE, null, ex);
            return false;
        }
    }
    
    /**
     * Collects tweets from Twitter's "garden hose" random stream.  Also
     * filters specific hash-tags into separate files for further processing.
     * 
     * @param count: total number of tweets in the sample.
     * @param tweetsPerFile: break the random stream down into more manageable
     * sized file (recommended to be around 1000 per file maximum).
     */
    static public void CollectRandomTweets(int count, int tweetsPerFile)
    {
        //  Running total
        int totalTweets = 0;

        try 
        {
            while (totalTweets < count) 
            {
                //  New stream collection batch
                StatusStream ss = new StatusStream(tweetsPerFile);

                //  Execute the collection
                int tweetsReceviedInLastBatch = ss.execute();
                
                //  Only write output if we returned some statuses
                if (tweetsReceviedInLastBatch > 0) 
                {
                    //  Get the random tweet stream and write to file
                    List<twitter4j.Status> statuses = ss.GetCollectedStatuses();
                    String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss")
                            .format(Calendar.getInstance().getTime());

                    WriteToMainOutput(timeStamp, statuses);
                    
                    WriteToHashTagOutput("poverty", timeStamp, 
                            ss.GetPovertyStatuses());
                    WriteToHashTagOutput("justice", timeStamp, 
                            ss.GetJusticeStatuses());
                    WriteToHashTagOutput("recession", timeStamp, 
                            ss.GetRecessionStatuses());
                    WriteToHashTagOutput("austerity", timeStamp, 
                            ss.GetAusterityStatuses());
                    WriteToHashTagOutput("compassion", timeStamp, 
                            ss.GetCompassionStatuses());
                    WriteToHashTagOutput("cuts", timeStamp, 
                            ss.GetCutsStatuses());
                    WriteToHashTagOutput("poor", timeStamp, 
                            ss.GetPoorStatuses());
                    WriteToHashTagOutput("minimumWage", timeStamp, 
                            ss.GetMinimumWageStatuses());
                    WriteToHashTagOutput("raiseTheWage", timeStamp, 
                            ss.GetRaiseTheWageStatuses());
                    WriteToHashTagOutput("livingWage", timeStamp, 
                            ss.GetLivingWageStatuses());
                    WriteToHashTagOutput("homeless", timeStamp, 
                            ss.GetHomelessStatuses());
                    WriteToHashTagOutput("lowWage", timeStamp, 
                            ss.GetLowWageStatuses());
                    
                    totalTweets += tweetsReceviedInLastBatch;
                }
            }
        } 
        catch (IllegalArgumentException 
                | InterruptedException 
                | TwitterException ex) 
        {
            JOptionPane.showMessageDialog(null, ex.toString(), "Exception", 
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Every tweet that is received from the random stream sample is written
     * to a time and date stamped file here.  Information relating to the tweet,
     * such as the originating account and hash-tags are also stored.  The file
     * is in comma-separated-value format (CSV) with one tweet per line.  This
     * file can them be further processed using other tools or examined using
     * a text editor or Microsoft Excel.
     * 
     * @param timeStamp: a string containing the time and date to be appended 
     * to a generic filename.
     * @param statuses: a list of multiple statuses to be stored.  It is 
     * recommended to stick to relatively small numbers per file so that tweets
     * are written often in the event of system errors.  Approximate file size
     * is 100KB/500 tweets.
     */
    static public void WriteToMainOutput(String timeStamp, 
            List<twitter4j.Status> statuses) 
    {
        String randomFilename = OUTPUT_DIR +
                RANDOM_TWEETS_FILENAME + "_" + timeStamp + ".csv";

        FileWriter randomStream;
        try {
            randomStream = new FileWriter(randomFilename);
            for (twitter4j.Status s : statuses) {
                //  First trim carrage returns from the tweets and names
                String tweetText = s.getText();
                tweetText = tweetText.replace("\n", "").replace("\r", "");
                String tweetName = s.getUser().getName();
                tweetName = tweetName.replace("\n", "").replace("\r", "");

                //  Build up a string for all information for the current
                //  tweet
                String line = "";
                line += s.getUser().getId();
                line += ",";
                line += tweetName;
                line += ",";
                line += s.getId();
                line += ",";
                line += tweetText;
                line += ",";
                line += s.getInReplyToStatusId();
                line += ",";
                line += s.getInReplyToUserId();
                line += ",";
                line += s.getInReplyToScreenName();
                line += ",";
                line += s.getCreatedAt();
                line += ",";
                line += s.isFavorited();
                line += ",";
                line += s.getFavoriteCount();
                line += ",";
                line += s.getRetweetCount();
                line += ",";

                for (HashtagEntity hashTag : s.getHashtagEntities()) {
                    line += hashTag.getText();
                    line += ",";
                }

                for (URLEntity url : s.getURLEntities()) {
                    line += url.getExpandedURL();
                    line += ",";
                }
                line += "\r\n";

                randomStream.write(line);
            }
            randomStream.close();
        } catch (IOException ex) {
            Logger.getLogger(RandomTweetDownloader.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
    }
    
    /**
     * 
     * @param hashTag
     * @param timeStamp
     * @param statuses 
     */
    static public void WriteToHashTagOutput(String hashTag, 
            String timeStamp, List<twitter4j.Status> statuses) 
    {
        String filename = OUTPUT_DIR + hashTag + ".csv";

        FileWriter fileWritter;
        try 
        {
            fileWritter = new FileWriter(filename, true);
            
            for (twitter4j.Status s : statuses) 
            {
                //  First trim carrage returns from the tweets
                String tweetText = s.getText();
                tweetText = tweetText.replace("\n", "").replace("\r", "");

                String tweetName = s.getUser().getName();
                tweetName = tweetName.replace("\n", "").replace("\r", "");

                String line = "";

                line += s.getUser().getId();
                line += ",";
                line += tweetName;
                line += ",";
                line += s.getId();
                line += ",";
                line += tweetText;
                line += ",";
                line += s.getInReplyToStatusId();
                line += ",";
                line += s.getInReplyToUserId();
                line += ",";
                line += s.getInReplyToScreenName();
                line += ",";
                line += s.getCreatedAt();
                line += ",";
                line += s.isFavorited();
                line += ",";
                line += s.getFavoriteCount();
                line += ",";
                line += s.getRetweetCount();
                line += ",";

                for (HashtagEntity hashTagEn : s.getHashtagEntities()) {
                    line += hashTagEn.getText();
                    line += ",";
                }

                for (URLEntity url : s.getURLEntities()) {
                    line += url.getExpandedURL();
                    line += ",";
                }
                line += "\r\n";

                fileWritter.write(line);
            }
            fileWritter.close();
        } 
        catch (IOException ex) 
        {
            Logger.getLogger(RandomTweetDownloader.class.getName()).log(
                    Level.SEVERE, null, ex);
        }

    }
}


