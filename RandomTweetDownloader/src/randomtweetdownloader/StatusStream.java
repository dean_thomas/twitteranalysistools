package randomtweetdownloader;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import twitter4j.HashtagEntity;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterException;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *
 *  http://stackoverflow.com/questions/18016532/stop-the-twitter-stream-and-return-list-of-status-with-twitter4j?rq=1
 */

/**
 *
 * @author Dean
 */
public class StatusStream 
{
    //  Access to the random Twitter feed via twitter4j
    private TwitterStream m_twitterStream = 
            new TwitterStreamFactory().getInstance();

    //  How many tweets are we aiming to collect?
    private int m_tweetTotal = 0;
    
    //  All the tweets collected
    private List<Status> m_allCollectedStatuses = new ArrayList<>();

    //  These lists keep track of statuses identified with
    //  specified keywords.  They are filled in the execute() method.
    private List<Status> m_povertyStatuses = new ArrayList<>();
    private List<Status> m_justiceStatuses = new ArrayList<>();
    private List<Status> m_recessionStatuses = new ArrayList<>();
    private List<Status> m_austerityStatuses = new ArrayList<>();
    private List<Status> m_compassionStatuses = new ArrayList<>();
    private List<Status> m_cutsStatuses = new ArrayList<>();
    private List<Status> m_poorStatuses = new ArrayList<>();
    private List<Status> m_minimumWageStatuses = new ArrayList<>();
    private List<Status> m_raiseTheWageStatuses = new ArrayList<>();
    private List<Status> m_livingWageStatuses = new ArrayList<>();
    private List<Status> m_homelessStatuses = new ArrayList<>();
    private List<Status> m_lowWageStatuses = new ArrayList<>();

    /**
     * Constructor.  Sets up the collection stream but does not begin capture.
     * @param tweetsToCollect: the total number of tweets to collect in this 
     * batch 
     */
    public StatusStream(int tweetsToCollect) {
        this.m_tweetTotal = tweetsToCollect;
    }
    
    /**
     * Access method
     * @return List<Status>: all tweets collected so far
     */
    public List<Status> GetCollectedStatuses() {
        return m_allCollectedStatuses;
    }

    /**
     * Collects a batch of tweets from the random feed.  The actual number 
     * collected may be less than this for a number of reasons.  Loss of 
     * Internet connection, Twitter traffic limitations and Twitter servers
     * becoming unreachable are examples of this.
     * 
     * @return int: the actual number of tweets collected in the batch
     * @throws TwitterException
     * @throws InterruptedException
     * @throws IllegalArgumentException 
     */
    public int execute() throws 
            TwitterException, InterruptedException, IllegalArgumentException 
    {
        final BlockingQueue<Status> bqAllStatuses = new LinkedBlockingQueue<>(10 * m_tweetTotal);
        
        //  Extend the functionality of the abstract StatusListener type in
        //  Twitter4j to carry out the tasks we require.
        StatusListener listener = new StatusListener() 
        {
            /**
             * A new status ('tweet') was received from the feed.  We can
             * use this to filter tweets as they appear.
             * 
             * @param status: the status to be processed
             */
            @Override
            public void onStatus(Status status) 
            {
                try 
                {
                    //  We only look for tweets marked as English language 'EN'
                    if ("en".equals(status.getLang())) 
                    {
                        //  Attempt to add the status to the main queue
                        bqAllStatuses.offer(status);

                        //  Check to see if the tweet contains a hashtag of 
                        //  interest to the collection.  If so this is also 
                        //  cloned to relevant hashtag specific lists.
                        for (HashtagEntity hashTag : status.getHashtagEntities()) 
                        {
                            //  All statuses are converted and tested in
                            //  lowercase, so that any combination of upper
                            //  and lowercase letters can be processed.
                            if (hashTag.getText().toLowerCase().
                                    contains("poverty")) 
                            {
                                m_povertyStatuses.add(status);
                            }
                            if (hashTag.getText().toLowerCase().
                                    contains("justice")) 
                            {
                                m_justiceStatuses.add(status);
                            }
                            if (hashTag.getText().toLowerCase().
                                    contains("recession")) 
                            {
                                m_recessionStatuses.add(status);
                            }
                            if (hashTag.getText().toLowerCase().
                                    contains("austerity")) 
                            {
                                m_austerityStatuses.add(status);
                            }
                            if (hashTag.getText().toLowerCase().
                                    contains("compassion")) {
                                m_compassionStatuses.add(status);
                            }
                            if (hashTag.getText().toLowerCase().
                                    contains("cuts")) {
                                m_cutsStatuses.add(status);
                            }
                            if (hashTag.getText().toLowerCase().
                                    contains("poor")) {
                                m_poorStatuses.add(status);
                            }
                            
                            //  Multiple variations of this tag will be
                            //  accepted
                            if (hashTag.getText().toLowerCase().
                                    contains("minimumwage") 
                                    || hashTag.getText().toLowerCase().
                                            contains("minimum_wage") 
                                    || hashTag.getText().toLowerCase().
                                            contains("minimum-wage")) 
                            {
                                m_minimumWageStatuses.add(status);
                            }
                            if (hashTag.getText().toLowerCase().
                                    contains("raisethewage") 
                                    || hashTag.getText().toLowerCase().
                                            contains("raise_the_wage") 
                                    || hashTag.getText().toLowerCase().
                                            contains("raise-the-wage")) 
                            {
                                m_raiseTheWageStatuses.add(status);
                            }
                            if (hashTag.getText().toLowerCase().
                                    contains("livingwage") 
                                    || hashTag.getText().toLowerCase().
                                            contains("living_wage") 
                                    || hashTag.getText().toLowerCase().
                                            contains("living-wage")) 
                            {
                                m_livingWageStatuses.add(status);
                            }
                            if (hashTag.getText().toLowerCase().
                                    contains("homeless")) 
                            {
                                m_homelessStatuses.add(status);
                            }
                            if (hashTag.getText().toLowerCase().
                                    contains("lowwage") 
                                    || hashTag.getText().toLowerCase().
                                            contains("low_wage") 
                                    || hashTag.getText().toLowerCase().
                                            contains("low-wage")) 
                            {
                                m_lowWageStatuses.add(status);
                            }
                        }
                    }

                } 
                catch (Exception ex) 
                {
                    Logger.getLogger(RandomTweetDownloader.class.getName()).
                            log(Level.SEVERE, null, ex);
                }
            }

            /**
             * Twitter event that me aren't tracking here
             * @param sdn 
             */
            @Override
            public void onDeletionNotice(StatusDeletionNotice sdn) { }

            /**
             * Twitter event that me aren't tracking here
             * @param i
             */
            @Override
            public void onTrackLimitationNotice(int i) { }

            /**
             * Twitter event that me aren't tracking here
             * @param l
             * @param l1
             */
            @Override
            public void onScrubGeo(long l, long l1) { }

            /**
             * Twitter event raised when the traffic limiting message is 
             * received.  Log this to the error console.
             * @param sw 
             */
            @Override
            public void onStallWarning(StallWarning sw) {
                System.err.println(sw.getMessage());
            }

            /**
             * An exception was raised from the interface
             * @param excptn 
             */
            @Override
            public void onException(Exception excptn) {
                throw new UnsupportedOperationException(excptn.getMessage()); 
                //To change body of generated methods, choose Tools | Templates.

            }
        };

        //  Number of null tweets counted
        int nullCount = 0;
        
        //  If we receive a total of 10 null statuses we will determine that
        //  the current collection has failed.
        boolean failed = false;
        
        //  Add the above listener to process tweets as they arrive and
        //  begin to sample the stream.
        m_twitterStream.addListener(listener);
        m_twitterStream.sample();
        
        //  Collections are tried continually.  If nothing happens in 30 
        //  seconds, we will timeout.  Keep repeating until we hit our target 
        //  (or fail).
        while ((m_allCollectedStatuses.size() < m_tweetTotal) && (!failed)) 
        {
            //  Current status
            Status status;

            try 
            {
                //  Try polling for a status for 30 seconds
                status = bqAllStatuses.poll(30, TimeUnit.SECONDS);

                if (status == null) 
                {
                    //  Last tweet collected was null - maybe we have exceeded
                    //  our download limit for now.                
                    nullCount++;
                    
                    //  10 nulls in a row.  Stopping try to collect in this 
                    //  batch and return what has been collected.
                    if (nullCount > 10)
                    {
                        System.err.println(
                                "Null count exceeded 10 - aborting collection");
                        
                        failed = true;
                    }
                }
                else
                {
                    //  Add the status to the random sample list
                    m_allCollectedStatuses.add(status);
                
                    failed = false;
                }
            } 
            catch (InterruptedException e) 
            {
                System.err.println(e.toString());
                
                //  Close the stream and retry
                //  and return to main application thread
                failed = true;
            }
        }
        
        //  Clear the blocking queue and close the twitter connection
        bqAllStatuses.clear();
        m_twitterStream.shutdown();

        //  Return how many tweets were sucessfully collected
        return m_allCollectedStatuses.size();
    }

    /**
     * @return the m_povertyStatuses
     */
    public List<Status> GetPovertyStatuses() {
        return m_povertyStatuses;
    }

    /**
     * @return the m_justiceStatuses
     */
    public List<Status> GetJusticeStatuses() {
        return m_justiceStatuses;
    }

    /**
     * @return the m_recessionStatuses
     */
    public List<Status> GetRecessionStatuses() {
        return m_recessionStatuses;
    }

    /**
     * @return the m_austerityStatuses
     */
    public List<Status> GetAusterityStatuses() {
        return m_austerityStatuses;
    }

    /**
     * @return the m_compassionStatuses
     */
    public List<Status> GetCompassionStatuses() {
        return m_compassionStatuses;
    }

    /**
     * @return the m_cutsStatuses
     */
    public List<Status> GetCutsStatuses() {
        return m_cutsStatuses;
    }

    /**
     * @return the m_poorStatuses
     */
    public List<Status> GetPoorStatuses() {
        return m_poorStatuses;
    }

    /**
     * @return the m_minimumWageStatuses
     */
    public List<Status> GetMinimumWageStatuses() {
        return m_minimumWageStatuses;
    }

    /**
     * @return the m_raiseTheWageStatuses
     */
    public List<Status> GetRaiseTheWageStatuses() {
        return m_raiseTheWageStatuses;
    }

    /**
     * @return the m_livingWageStatuses
     */
    public List<Status> GetLivingWageStatuses() {
        return m_livingWageStatuses;
    }

    /**
     * @return the m_homelessStatuses
     */
    public List<Status> GetHomelessStatuses() {
        return m_homelessStatuses;
    }

    /**
     * @return the m_lowWageStatuses
     */
    public List<Status> GetLowWageStatuses() {
        return m_lowWageStatuses;
    }
}
