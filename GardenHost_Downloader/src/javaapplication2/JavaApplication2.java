/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import randomtweetdownloader.StatusStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import twitter4j.HashtagEntity;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.URLEntity;

/**
 *
 * @author 4thFloor
 */
public class JavaApplication2 {

    static String randomFilenameBase = "random";

   public static void main(String[] args)
    {
        /*
        //  TODO: No error checks at the moment
        if (args[1].equalsIgnoreCase("-c"))
        {
            int count = Integer.parseInt(args[2]);
            int tweetsPerFile = Integer.parseInt(args[3]);
        
            CollectRandomTweets(count, tweetsPerFile);
        }
        */
        //LookupUser lookupUser = new LookupUser(234547837);
        UserQuery userQuery = new UserQuery();
        
        
        userQuery.ProcessHashtagFile(
                "C:\\twitterCorpus\\poor.CSV",
                "C:\\twitterCorpus\\poor_user.csv");
        
        //System.out.println(userQuery.GetUserInfoCSV("j_heino"));
        /*
        userQuery.ProcessFollowerListFile(
                "C:\\Users\\dean\\Dropbox\\Uni Work\\RA\\Twitter\\Corpus\\NodeXL\\Low Wage Dataset\\most_popular_user_in_hashtag_list_all_folowers.csv", 
                "C:\\Users\\dean\\Dropbox\\Uni Work\\RA\\Twitter\\Corpus\\NodeXL\\Low Wage Dataset\\most_popular_user_in_hashtag_list_all_folowers_processed.csv");
                */
    }
    
   /*
    static public void CollectRandomTweets(int count, int tweetsPerFile)
    {
        int totalTweets = 0;

        try {
            while (totalTweets < count) {
                PrintStream ps = new PrintStream("C:\\twitterCorpus\\logfile.log");
                System.setOut(ps);
        
                StatusStream ss = new StatusStream(tweetsPerFile);

                //  Execute the collection
                int tweetsReceviedInLastBatch = ss.execute();
                
                //  Only write output if we returned some statuses
                if (tweetsReceviedInLastBatch > 0) 
                {
                    List<Status> statuses = ss.allCollectedStatuses();
                    String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss").format(Calendar.getInstance().getTime());

                    WriteToMainOutput(timeStamp, statuses);
                    WriteToHashTagOutput("poverty", timeStamp, ss.povertyStatuses);
                    WriteToHashTagOutput("justice", timeStamp, ss.justiceStatuses);
                    WriteToHashTagOutput("recession", timeStamp, ss.recessionStatuses);
                    WriteToHashTagOutput("austerity", timeStamp, ss.austerityStatuses);
                    WriteToHashTagOutput("compassion", timeStamp, ss.compassionStatuses);
                    WriteToHashTagOutput("cuts", timeStamp, ss.cutsStatuses);
                    WriteToHashTagOutput("poor", timeStamp, ss.poorStatuses);
                    WriteToHashTagOutput("minimumWage", timeStamp, ss.minimumWageStatuses);
                    WriteToHashTagOutput("raiseTheWage", timeStamp, ss.raiseTheWageStatuses);
                    WriteToHashTagOutput("livingWage", timeStamp, ss.livingWageStatuses);
                    WriteToHashTagOutput("homeless", timeStamp, ss.homelessStatuses);
                    WriteToHashTagOutput("lowWage", timeStamp, ss.lowWageStatuses);

                    totalTweets += tweetsReceviedInLastBatch;
                }
            }
        } catch (FileNotFoundException | IllegalArgumentException | InterruptedException | TwitterException ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Exception", JOptionPane.ERROR_MESSAGE);
        }
    }


    static public void WriteToMainOutput(String timeStamp, List<Status> statuses) {
        String randomFilename = "C://twitterCorpus//" + randomFilenameBase + timeStamp + ".csv";

        FileWriter randomStream;
        try {
            randomStream = new FileWriter(randomFilename);
            for (Status s : statuses) {
                //  First trim carrage returns from the tweets
                String tweetText = s.getText();
                tweetText = tweetText.replace("\n", "").replace("\r", "");

                String tweetName = s.getUser().getName();
                tweetName = tweetName.replace("\n", "").replace("\r", "");

                String line = "";

                line += s.getUser().getId();
                line += ",";
                line += tweetName;
                line += ",";
                line += s.getId();
                line += ",";
                line += tweetText;
                line += ",";
                line += s.getInReplyToStatusId();
                line += ",";
                line += s.getInReplyToUserId();
                line += ",";
                line += s.getInReplyToScreenName();
                line += ",";
                line += s.getCreatedAt();
                line += ",";
                line += s.isFavorited();
                line += ",";
                line += s.getFavoriteCount();
                line += ",";
                line += s.getRetweetCount();
                line += ",";

                for (HashtagEntity hashTag : s.getHashtagEntities()) {
                    line += hashTag.getText();
                    line += ",";
                }

                for (URLEntity url : s.getURLEntities()) {
                    line += url.getExpandedURL();
                    line += ",";
                }
                line += "\r\n";

                randomStream.write(line);
            }
            randomStream.close();
        } catch (IOException ex) {
            Logger.getLogger(JavaApplication2.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    */
    static public void WriteToHashTagOutput(String hashTag, String timeStamp, List<Status> statuses) {
        String filename = "C://twitterCorpus//" + hashTag + ".csv";

        FileWriter randomStream;
        try {
            randomStream = new FileWriter(filename, true);
            for (Status s : statuses) {
                //  First trim carrage returns from the tweets
                String tweetText = s.getText();
                tweetText = tweetText.replace("\n", "").replace("\r", "");

                String tweetName = s.getUser().getName();
                tweetName = tweetName.replace("\n", "").replace("\r", "");

                String line = "";

                line += s.getUser().getId();
                line += ",";
                line += tweetName;
                line += ",";
                line += s.getId();
                line += ",";
                line += tweetText;
                line += ",";
                line += s.getInReplyToStatusId();
                line += ",";
                line += s.getInReplyToUserId();
                line += ",";
                line += s.getInReplyToScreenName();
                line += ",";
                line += s.getCreatedAt();
                line += ",";
                line += s.isFavorited();
                line += ",";
                line += s.getFavoriteCount();
                line += ",";
                line += s.getRetweetCount();
                line += ",";

                for (HashtagEntity hashTagEn : s.getHashtagEntities()) {
                    line += hashTagEn.getText();
                    line += ",";
                }

                for (URLEntity url : s.getURLEntities()) {
                    line += url.getExpandedURL();
                    line += ",";
                }
                line += "\r\n";

                randomStream.write(line);
            }
            randomStream.close();
        } catch (IOException ex) {
            Logger.getLogger(JavaApplication2.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
