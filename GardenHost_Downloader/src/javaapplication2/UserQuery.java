/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;

/**
 *
 * @author 4thFloor
 */
public class UserQuery {

    Twitter twitter;

    public UserQuery() {
        twitter = new TwitterFactory().getInstance();

    }

    public String GetUserInfoCSV(String userName) {
        try {
            User user = twitter.showUser(userName);

            if (null != user) {
                return userInfoInCSV(user);
            } else {
                return "";
            }
        } catch (TwitterException ex) {
            Logger.getLogger(UserQuery.class.getName()).log(Level.SEVERE, null, ex);

            return "";
        }
    }

    public String GetUserInfoCSV(long userID) {
        try {

            User user = twitter.showUser(userID);

            if (null != user) {
                return userInfoInCSV(user);
            } else {
                return "";
            }
        } catch (TwitterException ex) {

            if (ex.exceededRateLimitation()) {
                System.err.println("Rate limit hit.  Waiting 15 mins");
                try {
                    Thread.sleep(15 * 60 * 1000);
                    
                    return GetUserInfoCSV(userID);
                } catch (InterruptedException ex1) {
                    Logger.getLogger(UserQuery.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            return "NOT FOUND";
        }
    }

    private float tweetsPerDay(Date signup, int tweetCount) {
        Date now = new Date();
        //Date diff = now - signup;
        float days = (((now.getTime()) - signup.getTime()) / (1000 * 60 * 60 * 24));

        return tweetCount / days;

    }

    private String userInfoInCSV(User user) {
        String result = "";

        result += user.getId() + ",";
        result += "@" + user.getScreenName().replace(",","_").replace("\r","").replace("\n","") + ",";
        result += user.getName().replace(",","_").replace("\r","").replace("\n","") + ",";
        result += user.getDescription().replace(",","_").replace("\r","").replace("\n","") + ",";
        //  Join date
        result += user.getCreatedAt() + ",";
        //  Number of tweets
        result += user.getStatusesCount() + ",";
        //  Tweets per day
        result += tweetsPerDay(user.getCreatedAt(), user.getStatusesCount()) + ",";
        //  Verified status
        result += user.isVerified() + ",";
        //  Number of followers
        result += user.getFollowersCount() + ",";
        //  Number of people following
        result += user.getFriendsCount() + ",";
        //  Listed count
        result += user.getListedCount() + ",";
        //  Number of user favourited tweets
        result += user.getFavouritesCount() + ",";
        //  Privacy setting is protected
        result += user.isProtected() + "";

        return result;
    }

    private String getOutputHeader()
    {
        return "UserID, ScreenName, Name, Description, Account date, Number of tweets, Tweets per day, Verified, Follower count, Following, Listed count, user favourites, protected";
    }
    
    /**
     * Given a CSV file of follower account names, return a detailed list of 
     * all user accounts.  This will contain advanced data about the users 
     * including name, tweets per day, follower count etc.
     * 
     * @param inputFilename the list of accounts to lookup
     * @param outputFilename the output file containing extra details of the
     * account contained in the input file
     */
    public void ProcessFollowerListFile(String inputFilename, String outputFilename)
    {
        ArrayList<String> outputFileLines = new ArrayList<>();
        
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inputFilename));

            String currentLine;

            while ((currentLine = reader.readLine()) != null) {
                //  Split the current line using comma separators
                String[] currentLineValues = currentLine.split(",");

                //  User name is the first value
                String userName = currentLineValues[0];

                //  Add the user data to the output file lines
                outputFileLines.add(GetUserInfoCSV(userName));
            }

            reader.close();

            FileWriter writer = new FileWriter(outputFilename);

            writer.write(getOutputHeader());
            writer.write("\r\n");
            
            for (int i = 0; i < outputFileLines.size(); ++i) {
                writer.write(outputFileLines.get(i));
                writer.write("\r\n");
            }

            writer.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserQuery.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserQuery.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Take a list of tweets identified in a data set as a containing a
     * specific hashtag, look up each of the users and output their account
     * details to second CSV formatted file.  This will contain advanced data 
     * about the users including name, tweets per day, follower count etc.
     * 
     * @param inputFilename the file containing all the tweets of a hashtag 
     * dataset
     * @param outputFilename the file to be written giving details of each of
     * the user's accounts in the data set
     */
    public void ProcessHashtagFile(String inputFilename, String outputFilename) {
        ArrayList<String> outputFileLines = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(inputFilename));

            String currentLine;

            while ((currentLine = reader.readLine()) != null) {
                //  Split the current line using comma separators
                String[] currentLineValues = currentLine.split(",");

                //  User id is the first value
                long userId = Long.parseLong(currentLineValues[0]);

                //  Add the user data to the output file lines
                outputFileLines.add(GetUserInfoCSV(userId));
            }

            reader.close();

            FileWriter writer = new FileWriter(outputFilename);

            writer.write(getOutputHeader());
            writer.write("\r\n");
            
            for (int i = 0; i < outputFileLines.size(); ++i) {
                writer.write(outputFileLines.get(i));
                writer.write("\r\n");
            }

            writer.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserQuery.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserQuery.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    
}
